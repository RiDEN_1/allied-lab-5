package controls;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.testng.IClassListener;
import ru.yandex.qatools.htmlelements.element.Link;

public class WebLink extends  WebTypifiedElement implements IClick {
    private static final Logger log = LogManager.getLogger(WebLink.class);
    public WebLink(WebElement wrappedElement) {
        super(wrappedElement);
    }
    public String getReference() {
        log.debug("Getting reference for link " + getName() + "element");
        return getWrappedElement().getAttribute("href");
    }
    @Override
    public void click() {
        getWrappedElement().click();
        log.debug("Clicked on " + getName() + " link.");
    }
}
