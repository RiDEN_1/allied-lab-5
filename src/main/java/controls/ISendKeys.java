package controls;

public interface ISendKeys {
    public void sendKeys(CharSequence... keysToSend);
}
