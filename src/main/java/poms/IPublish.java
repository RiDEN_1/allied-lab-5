package poms;

public interface IPublish {
    void publish() throws InterruptedException;
    void checkPresence(String title);
}
